//  Copyright © 2019 iProgram. All rights reserved.

import Foundation
import Core

class RepoEndpoint: Endpoint {
    var version: Double { 2.0 }
    var name: String { "repositories" }
}
