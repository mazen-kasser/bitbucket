//  Copyright © 2019 iProgram. All rights reserved.

import UIKit
import Core
import ReposKit

class RepoCell: UITableViewCell {

    @IBOutlet private weak var displayNameLabel: UILabel!
    @IBOutlet private weak var typeLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var createDateLabel: UILabel!
    
    var repo: Repo? {
        didSet {
            guard let repo = repo else { return }
            
            displayNameLabel.text = repo.ownerDisplayName
            typeLabel.text = repo.ownerType
            createDateLabel.text = repo.dateOfCreation
            
            // load images async
            guard let avatarUrl = repo.avatarUrl else { return }
            avatarImageView.load(url: avatarUrl)
        }
    }
    
}
