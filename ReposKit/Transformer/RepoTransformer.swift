//  Copyright © 2019 iProgram. All rights reserved.

import Foundation

class RepoTransformer {
    /// Transform [RepoDto] -> [Repo].
    static func transform(from repos: [RepoDto]) -> [Repo] {
        
        return repos.map { repoDto in
            Repo(
                ownerDisplayName: repoDto.owner.displayName,
                ownerType: repoDto.owner.type,
                dateOfCreation: DateFormatter.yyyyMMdd.string(from: repoDto.createdOn),
                avatarUrl: transform(from: repoDto.owner.links["avatar"]?.href)
            )
        }
    }
    
    static func transform(from path: String?) -> URL? {
        return URL(string: path ?? "")
    }
}

fileprivate extension DateFormatter {
  static let yyyyMMdd: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.calendar = Calendar(identifier: .iso8601)
    return formatter
  }()
}
