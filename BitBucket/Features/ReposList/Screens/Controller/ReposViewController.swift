//  Copyright © 2019 iProgram. All rights reserved.

import UIKit
import Core

class ReposViewController: UITableViewController {

    private var viewModel = ReposViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(RepoCell.self)
        tableView.tableFooterView = UIView()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        refreshData()
    }
    
    @objc
    private func refreshData() {
        tableView.refreshControl?.beginRefreshing()
        viewModel.getRepos {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                
                self.title = String(describing: self.viewModel.repos.count) + " Repos"
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RepoCell.self)
        cell.repo = viewModel.repos[indexPath.row]
        return cell
    }

}

