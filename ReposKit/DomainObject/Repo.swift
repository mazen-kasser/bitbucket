//  Copyright © 2019 iProgram. All rights reserved.

import Foundation

public struct Repo: Decodable {
    let ownerDisplayName: String
    let ownerType: String
    let dateOfCreation: String
    let avatarUrl: URL?
}
