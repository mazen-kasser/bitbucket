//  Copyright © 2019 iProgram. All rights reserved.

import Foundation
import Core

struct RepoDtoResponse: Decodable {
    let repos: [RepoDto]
    let next: String?   // TODO: pagination
    
    enum CodingKeys: String, CodingKey {
        case repos = "values"
        case next
    }
}

struct RepoDto: Decodable {
    let createdOn: Date
    let owner: OwnerDto
}

struct OwnerDto: Decodable {
    struct Link: Decodable {
        let href: String
    }
    let displayName: String
    let type: String
    let links: [String: Link]
}
