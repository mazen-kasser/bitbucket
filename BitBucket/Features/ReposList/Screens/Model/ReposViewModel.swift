//  Copyright © 2019 iProgram. All rights reserved.

import Foundation
import ReposKit

class ReposViewModel {
    
    private var service: RepoService!
    
    init(service: RepoService = DefaultRepoService()) {
        self.service = service
    }
    
    var repos: [Repo] = []
    
    func getRepos(completion: @escaping () -> ()) {
        service.fetchRepos { repos  in
            self.repos = repos
            completion()
        }
    }
    
}
