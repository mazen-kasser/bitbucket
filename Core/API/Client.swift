//  Copyright © 2019 iProgram. All rights reserved.

import Foundation

// MARK: Endpoint

public protocol Endpoint {
    var version: Double { get }
    var name: String { get }
}

// MARK: Client

public protocol Client {
    var basePath: String { get }
    var endpoint: Endpoint { get }
}

public extension Client {
    var path: URL { URL(string: "\(basePath)/\(endpoint.version)/\(endpoint.name)")! }
    
    func fetch(completion: @escaping (Data) -> ()) {
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: path) { (data, response, error) in
            guard let data = data, error == nil else { return }
            completion(data)
        }
        task.resume()
    }
}
