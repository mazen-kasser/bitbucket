# BitBucket Repositories

## Overview

* This is a Swift app, use Xcode 11 support iOS 11.0 or higher
* Used suitable design patterns, and strict separation of consern
* Created library containing all functionality that could be reused by 3rd party developers in any application that requires pull bitbucket repositories list.
* Asynchronous development principals when retrieving and displaying data originating from network calls
* UI interaction and data binding principals 
* The app is built with a universal UI
* Incremental Commits of code

## Features
 
* App Target: `BitBucket` 
* Frameworks: `ReposKit` and `Core`
* Unit test suite for each

## Patterns

* Data transfer object, transformer, and domain object. This has been used at the service layer to mimic the API response and map it to a UI related object
* Coordinator pattern not needed at this stage, as there were only one screen
* MVC pattern been used as fit for purpose for the requirement of this exercise

## App Structure

Shows the dependencies unidirectional flows from top to bottom.

* Core.framework
    |_ NetworkClient
    |_ Helpers
    
    * ReposKit.framework
        |_ Service
            |_ Endpoint
        |_ DataTransferObject
        |_ Transformer
        |_ DomainObject

    * MainApp
         |_ AppDelegate
         |_ API
         |_ Features
            |_ FeatureName
                |_ Screens
                    |_ Controller
                    |_ Model
                    |_ View

## TODO

* Unit tests/mocks to demonstrate the code is testable
* Analytics to track usage of each Screen / Feature
* Logger to log warning and crashes
* Pagination to consume the `next` endpoint
* Bouns, would be to display more data
