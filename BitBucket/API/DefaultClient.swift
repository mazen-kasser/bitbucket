//  Copyright © 2019 iProgram. All rights reserved.

import Foundation
import Core

enum DefaultClient: Client {
    
    var basePath: String { "https://api.bitbucket.org" }
    
    case repos(endpoint: Endpoint)
    
    var endpoint: Endpoint {
        switch self {
        case .repos(let endpoint):
            return endpoint
        }
    }
}
