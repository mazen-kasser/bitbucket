//  Copyright © 2019 iProgram. All rights reserved.

import Foundation
import Core

protocol RepoService {
    func fetchRepos(endpoint: Endpoint, completion: @escaping ([Repo]) -> ())
}

extension RepoService {
    public func fetchRepos(completion: @escaping ([Repo]) -> ()) {
        fetchRepos(endpoint: RepoEndpoint(), completion: completion)
    }
}

public struct DefaultRepoService: RepoService {
    
    func fetchRepos(endpoint: Endpoint, completion: @escaping ([Repo]) -> ()) {
        DefaultClient.repos(endpoint: endpoint).fetch { data in
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let response = try decoder.decode(RepoDtoResponse.self, from: data)
                let repos = RepoTransformer.transform(from: response.repos)
                completion(repos)
            } catch {
                print("Decoding error ❗️\(error)")
            }
        }
    }

}


extension DateFormatter {
  static let iso8601Full: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
}
